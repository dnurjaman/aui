'use strict';

import createElement from '../../../../src/js/aui/create-element';
import globalize from '../../../../src/js/aui/internal/globalize';

describe('aui/internal/globalize', function () {
    var oldAjs;

    beforeEach(function () {
        oldAjs = window.AJS;
        window.AJS = undefined;
    });

    afterEach(function () {
        window.AJS = oldAjs;
    });

    it('should create the AJS global namespace if it does not exist', function () {
        expect(window.AJS).to.be.undefined;
        globalize('test', true);
        expect(window.AJS).to.not.be.undefined;
    });

    it('should make the AJS global namespace from the create-element module', function () {
        globalize('test', true);
        expect(window.AJS).to.equal(createElement);
    });

    it('should create a property on the global namespace using the name and value passed in', function () {
        globalize('test', true);
        expect(window.AJS.test).to.equal(true);
    });

    it('should return the namespaced value that was passed in', function () {
        expect(globalize('test', true)).to.equal(true);
    });
});
