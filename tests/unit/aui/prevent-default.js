'use strict';

import $ from '../../../src/js/aui/jquery';
import preventDefault from '../../../src/js/aui/prevent-default';

describe('aui/prevent-default', function () {
    it('globals', function () {
        expect(AJS.preventDefault).to.equal(preventDefault);
    });

    describe('API', function () {
        it('on click event and confirm event is prevented', function () {
            var e = $.Event('click');
            preventDefault(e);
            expect(e.isDefaultPrevented()).to.be.true;
        });
    });
});
