'use strict';

import '../../../src/js/aui/dropdown2';
import '../../../src/js/aui/header';
import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';
import headerComponent from '../../../src/js/aui/header';
import skate from 'skatejs';
import template from 'skatejs-template-html';

var $window = $(window);
var clock;

function resizeWindow (width, next) {
    $('#test-fixture').width(width);
    $window.trigger('resize');
    clock.tick(200);
    if (next) {
        helpers.afterMutations(next);
    }
}

function expectTriggerAndDropdownToNotExist() {
    var moreDropdownTriggerEl = document.getElementById('aui-responsive-header-dropdown-0-trigger');
    var moreDropdownEl = document.getElementById('aui-responsive-header-dropdown-0');
    expect(moreDropdownTriggerEl).to.not.exist;
    expect(moreDropdownEl).to.not.exist;
}

describe('Responsive header - ', function () {
    // Since the nav item <li>'s may be detatched from the DOM, save a reference to them.
    var $navItems;
    var header;

    beforeEach(function (done) {
        clock = sinon.useFakeTimers();
        header = helpers.ensureHtmlElement('<aui-header></aui-header>');
        header.id = 'test-header';
        skate.init(header);
        header.setAttribute('responsive', 'true');
        template.wrap(header).innerHTML = `
            <ul class="aui-nav aui-header-content">
                <li id="item-1" class="test-header-item"><a class="aui-nav-link" href="#item-1-href">Some long text (1)</a></li>
                <li id="item-2" class="test-header-item"><a class="aui-nav-link" href="#item-2-href">Some long text (2)</a></li>
                <li id="item-3" class="test-header-item"><a class="aui-nav-link" href="#item-3-href">Some long text (3)</a></li>
                <li id="item-4" class="test-header-item">
                    <a class="aui-dropdown2-trigger aui-style-default" href="#test-menu" aria-controls="test-menu" aria-haspopup="true" >Some long text (4)</a>
                    <div id="test-menu" class="aui-dropdown2 aui-style-default">
                        <ul>
                            <li><a href="//www.google.com">Google</a></li>
                        </ul>
                    </div>
                </li>
            </ul>`;
        skate.init(header.querySelector('.aui-nav'));

        helpers.fixtures({
            header: header
        });

        $navItems = $('#test-header .aui-nav > li');
        // afterMutations to allow the mutation observer to run <aui-header>'s attached callback.
        helpers.afterMutations(done);
    });

    afterEach(function () {
        $('#test-fixture').width('auto');
        clock.restore();
    });

    function getMoreDropdownItemEl ($navItem) {
        var $navItemTrigger = $navItem.children('a');
        var $moreDropdown = $('#aui-responsive-header-dropdown-0');
        if ($navItemTrigger.hasClass('aui-dropdown2-trigger')) {
            var dropdownId = $navItemTrigger.attr('aria-controls');
            return $moreDropdown.find(`aui-item-link[for="${dropdownId}"]`)[0];
        } else {
            return $moreDropdown.find(`aui-item-link[href="${ $navItemTrigger.attr('href') }"]`)[0];
        }
    }

    it('trigger and dropdown elements are lazily created', function () {
        expectTriggerAndDropdownToNotExist();
    });

    function expectItemIsInHeader ($navItem) {
        expect(document.getElementById($navItem.attr('id'))).to.equal($navItem[0]);
        expect(getMoreDropdownItemEl($navItem)).to.not.be.ok;
    }

    function expectItemIsInResponsiveDropdown ($navItem) {
        var $navItemTrigger = $navItem.children('a');
        var moreDropdownItemEl = getMoreDropdownItemEl($navItem);

        expect(document.getElementById($navItem.attr('id'))).to.not.be.ok;
        expect(moreDropdownItemEl.textContent).to.equal($navItemTrigger.text());
        expect(moreDropdownItemEl.getAttribute('href')).to.equal($navItemTrigger.attr('href'));
        if ($navItemTrigger.hasClass('aui-dropdown2-trigger')) {
            expect(moreDropdownItemEl.getAttribute('for')).to.equal($navItemTrigger.attr('aria-controls'));
        }
    }

    function $getMoreTrigger () {
        return $('#test-header .aui-header-primary .aui-nav > li:last .aui-dropdown2-trigger:not([aria-controls=test-menu])');
    }

    describe('when all items fit and when the container is resized smaller', function () {
        // More menu width is always the same. Unable to grab the width dynamically because the more menu is
        // inserted after the resize not before, hence calling width will get an incorrect value.
        var moreMenuWidth = 100;
        beforeEach(function (done) {
            var itemWidth = document.querySelector('.test-header-item').offsetWidth;
            var logo = $('#logo');
            var padding = logo.offset().left * 2 + logo.outerWidth(true) + moreMenuWidth;
            resizeWindow(itemWidth * 2 + padding, done);
        });

        it('the trigger for the responsive menu should be visible', function () {
            expect(document.querySelector('#aui-responsive-header-dropdown-0-trigger')).to.be.visible;
        });

        it('the header contains items 1 and 2, the responsive menu contains items 3 and 4', function () {
            expectItemIsInHeader($navItems.filter('#item-1'));
            expectItemIsInHeader($navItems.filter('#item-2'));
            expectItemIsInResponsiveDropdown($navItems.filter('#item-3'));
            expectItemIsInResponsiveDropdown($navItems.filter('#item-4'));
        });
    });

    describe('when only some items fit and when the container is resized larger', function () {
        beforeEach(function (done) {
            var itemWidth = document.querySelector('.test-header-item').offsetWidth;
            resizeWindow(itemWidth * 3, function () {
                //couldn't find a reliable way that works cross browser (auto and 100% didn't seem to work in phantomJS)
                //so we just set it to something very large
                resizeWindow('9999', done);
            });
        });

        it('the responsive menu trigger should not be visible', function () {
            expect(document.querySelector('#aui-responsive-header-dropdown-0 > a')).to.not.be.visible;
        });

        it('the header contains items 1, 2, 3, and 4', function () {
            expectItemIsInHeader($navItems.filter('#item-1'));
            expectItemIsInHeader($navItems.filter('#item-2'));
            expectItemIsInHeader($navItems.filter('#item-3'));
            expectItemIsInHeader($navItems.filter('#item-4'));
        });
    });

    describe('Submenus -', function () {
        function expectDropdownOpenedSideways($trigger) {
            var $dropdown = $('#' + $trigger.attr('aria-controls'));
            expect($trigger.hasClass('aui-dropdown2-sub-trigger')).to.equal(true, 'trigger');
            expect($dropdown.hasClass('aui-dropdown2-sub-menu')).to.equal(true, 'dropdown');
            expect($dropdown.attr('data-aui-alignment')).to.equal('submenu auto');
        }

        function expectDropdownOpenedDownwards($trigger) {
            var $dropdown = $('#' + $trigger.attr('aria-controls'));
            expect($trigger.hasClass('aui-dropdown2-sub-trigger')).to.equal(false, 'trigger');
            expect($dropdown.hasClass('aui-dropdown2-sub-menu')).to.equal(false, 'dropdown');
            expect($dropdown.attr('data-aui-alignment')).to.equal('bottom auto');
        }

        var itemWidth;
        var $navItemTrigger;
        var $navItemDropdown;
        beforeEach(function () {
            itemWidth = document.querySelector('.test-header-item').offsetWidth;
            $navItemTrigger = $navItems.filter('#item-4').find('.aui-dropdown2-trigger');
            $navItemDropdown = $('#' + $navItemTrigger.attr('aria-controls'));
        });

        [true, false].forEach(function (openBeforeResizing) {
            it(openBeforeResizing ? 'with' : 'without' + ' opening before resizing, opens as a submenu then downwards', function (done) {
                if (openBeforeResizing) {
                    helpers.click($navItemTrigger);
                    expectDropdownOpenedDownwards($navItemTrigger);
                    helpers.click($navItemTrigger);
                }
                resizeWindow(itemWidth * 3, function () {
                    var $moreItemLinkAnchor = $(`aui-item-link[for="${ $navItemDropdown.attr('id') }"] > a`);

                    helpers.click($getMoreTrigger());
                    helpers.click($moreItemLinkAnchor);
                    expectDropdownOpenedSideways($moreItemLinkAnchor);
                    helpers.click($getMoreTrigger());

                    resizeWindow('9999', function () {
                        helpers.click($navItemTrigger);
                        expectDropdownOpenedDownwards($navItemTrigger);
                        done();
                    });
                });
            });
        });
    });
});

describe('Responsive header with no nav items -', function () {
    var header;

    beforeEach(function () {
        clock = sinon.useFakeTimers();
    });

    beforeEach(function () {
        clock = sinon.useFakeTimers();
        header = helpers.ensureHtmlElement('<aui-header></aui-header>');
        header.id = 'test-header';
        header.setAttribute('responsive', 'true');
        template.wrap(header).innerHTML = `
            <ul class="aui-nav aui-header-content">
                <li id="item-1" class="test-header-item"><a class="aui-nav-link" href="#item-1-href">Some long text (1)</a></li>
                <li id="item-2" class="test-header-item"><a class="aui-nav-link" href="#item-2-href">Some long text (2)</a></li>
                <li id="item-3" class="test-header-item"><a class="aui-nav-link" href="#item-3-href">Some long text (3)</a></li>
                <li id="item-4" class="test-header-item">
                    <a class="aui-dropdown2-trigger aui-style-default" href="#test-menu" aria-controls="test-menu" aria-haspopup="true" >Some long text (4)</a>
                    <div id="test-menu" class="aui-dropdown2 aui-style-default">
                        <ul>
                            <li><a href="//www.google.com">Google</a></li>
                        </ul>
                    </div>
                </li>
            </ul>`;

        helpers.fixtures({
            header: header
        });

        skate.init(header);
    });

    afterEach(function () {
        $('#test-fixture').width('auto');
        clock.restore();
    });

    describe('no <ul class="aui-nav">', function () {
        beforeEach(function (done) {
            header = helpers.ensureHtmlElement('<aui-header></aui-header>');
            header.id = 'test-header';
            header.setAttribute('responsive', 'true');

            helpers.fixtures({
                header: header
            });

            skate.init(header);
            resizeWindow(500, done);
        });

        it('does not init', function () {
            expectTriggerAndDropdownToNotExist();
        });
    });

    describe('empty <ul class="aui-nav">', function () {
        beforeEach(function (done) {
            header = helpers.ensureHtmlElement('<aui-header></aui-header>');
            header.id = 'test-header';
            header.setAttribute('responsive', 'true');
            template.wrap(header).innerHTML = '<ul class="aui-nav aui-header-content"></ul>';

            helpers.fixtures({
                header: header
            });

            skate.init(header);
            resizeWindow(500, done);
        });

        it('does not init', function () {
            expectTriggerAndDropdownToNotExist();
        });
    });

    describe('hidden secondary nav (AUI-3550)', function () {
        beforeEach(function (done) {
            header = helpers.ensureHtmlElement('<aui-header></aui-header>');
            header.id = 'test-header';
            header.setAttribute('responsive', 'true');
            template.wrap(header).innerHTML = `
                <ul class="aui-nav aui-header-content"></ul>
                <div class="aui-header-secondary">
                    <ul class="aui-nav" style="display: none;">
                        <li><a href="#log-out">Log out</a></li>
                    </ul>
                </div>`;

            helpers.fixtures({
                header: header
            });

            skate.init(header);
            resizeWindow(500, done);
        });

        it('does not init', function () {
            expectTriggerAndDropdownToNotExist();
        });
    })
});
