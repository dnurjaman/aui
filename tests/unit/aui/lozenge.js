'use strict';

import AJS from '../../../src/js/aui';
import lozenge from '../../../src/js/aui/lozenge';
import helpers from '../../helpers/all';

describe('aui/lozenge', function () {
    var testLozenge = lozenge({
        display: '',
        type: ''
    });

    it('global', function () {
        expect(AJS.lozenge).to.equal(lozenge);
    });

    it('Lozenge element has prototype', function () {
        helpers.testPrototype(testLozenge, ['display', 'type']);
    });
});
