'use strict';

import drawLogo from '../../../src/js/aui/draw-logo';

describe('aui/draw-logo', function () {
    it('globals', function () {
        expect(AJS.drawLogo).to.equal(drawLogo);
    });
});
