Thanks for contributing to AUI! Here's a guide detailing some parts of our process to help you along the way

# Version
## Is the PR targeted at the branch for the correct AUI version? (Could be master or another stable branch)

If you want a change to a particular stable branch of AUI (eg. 5.9.x) ensure that you target that branch. [We follow semver](http://semver.org/).

## Do the changes need to be merged up to other stable branches?

If you are targeting a branch that isn't master, when merging, ensure that the changes are merged up to the correct branches (eg. 5.7.x -> 5.8.x -> 5.9.x -> master) 

# Code hygiene
## Are all strings internationalised?

## Does it adhere to code quality guidelines?

We are using eslint, it's recommended that you hook this up to your editor. We also have precommit hooks that check code changes for eslint problems.

# Code location
## Does this need a change in AUI-ADG?

It might need a corresponding change in the [AUI-ADG repo](https://bitbucket.org/atlassian/aui-adg) if it affects the AUI plugin, or is related to Atlassian-specific CSS (eg. Icons)



# Accessibility
## Does this need an accessibility audit? Is there an issue for that?

# Testing

## Does it needs unit tests?

## Were the proper unit tests written?

The unit tests should cover implicit expectations about behaviour, and anything that is documented should be tested.

## Do the tests pass?

## Does it need test-page examples?

In general, it's preferable to put examples in our documentation pages. However, for a few edge cases (bulletproofing that cannot be done in an automated test), test pages may be needed.

## Does it work in browsers other than Chrome? (IE 10+, Firefox, Safari)

We use [saucelabs](https://saucelabs.com/u/atlassian-aui) for cross-browser testing. At the least, automated tests should pass in Firefox and Chrome, and it should work as expected in other browsers.

# Documentation
## Does this need a changelog entry?

See `changelog.md`. We are following http://keepachangelog.com/

## Does it need new docs?

## Do the current docs need to be updated?

## Will this deprecate anything? Is that thing marked as deprecated (in code and in docs)?

# Making the PR

## Choose the reviewers

Choose two reviewers. [Design platform team members](https://bitbucket.org/designplatform/profile/members) are good candidates to put on the PR.

# Commits
## Is the branch up to date with the target branch?

## Merging

It is the responsibility of the author to merge the PR. However, if you are not on the design platform team, feel free to leave the PR and a team member will update it for you. 