'use strict';

import $ from 'jquery';
import skate from 'skatejs';

/**
 * Quick-search element that wraps a Single select component.
 *
 * Displays matches in a dropdown box as the user types, and hides the dropdown when the input is empty.
 *
 * Example:
 * <aui-quicksearch>
 *     <aui-option value="path/to/page/for/option.html">My option</aui-option>
 *     ..
 * </aui-quicksearch>
 *
 * The contents of the `<aui-quicksearch>` should be a collection of `<aui-option>` elements.
 * Each `<aui-option>` needs to provide the path to the target page for the option using the `value` attribute.
 */
skate('aui-quicksearch', {
    render: function (el) {
        el.innerHTML = `
            <form class="aui aui-quicksearch">
                <aui-select placeholder="Quick search">
                    ${el.innerHTML}
                </aui-select>
            </form>`;
    },
    created: function (el) {
        el._singleSelect = el.querySelector('aui-select');
        el._$popover = $(el).find('.aui-popover');
        el._$popover.addClass('aui-quicksearch-hidden');
    },
    events: {
        change: function (el) {
            var href = el._singleSelect.value;
            if (href) {
                window.location = href;
            }
        },
        input: function (el) {
            // Hack: Hide the popover if the input is empty
            if (!el.displayValue.trim()) {
                el._$popover.addClass('aui-quicksearch-hidden');
            } else {
                el._$popover.removeClass('aui-quicksearch-hidden');
            }
        },
        submit: function (el, e) {
            e.preventDefault();
        }
    },
    prototype: {
        get value() {
            return this._singleSelect.value;
        },
        get displayValue() {
            return this._singleSelect.displayValue;
        }
    }
});
