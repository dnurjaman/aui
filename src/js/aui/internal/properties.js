import assign from 'object-assign';
import attributes from './attributes';

function prop (def) {
    return function (...args) {
        args.unshift({}, def);
        return assign.apply(null, args);
    };
}

/**
    This property extension can be used with skate.
    Usage:
    ```
    properties: {
        respondsTo: properties.enum({values: ['toggle', 'hover'], missingDefault: 'toggle', invalidDefault: 'toggle'})({

        })
    }
    ```
 */
export default {
    'enum': function (enumOptions) {
        return prop({
            coerce: value => attributes.computeEnumValue(enumOptions, value),
            default: enumOptions.missingDefault,
            deserialize: value => value === null ? undefined : value,
            serialize: value => typeof value === 'undefined' ? value : String(value)
        })
    }
};
