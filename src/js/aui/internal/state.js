'use strict';

const propName = '__state';

export default function (element) {
    return {
        set (name, value) {
            if (!element[propName]) {
                element[propName] = {};
            }
            element[propName][name] = value;
            return this;
        },
        get (name) {
            if (element[propName]) {
                return element[propName][name];
            }
        },
        remove (name) {
            if (element[propName]) {
                delete element[propName][name];
            }
            return this;
        }
    };
};
