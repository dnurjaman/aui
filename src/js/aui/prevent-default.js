'use strict';

import globalize from './internal/globalize';

/**
 * Calls e.preventDefault. This is designed for event handlers that only need to prevent the default browser
 * action, eg:
 *
 *     $(".my-class").click(AJS.preventDefault)
 *
 * @param {jQuery.Event} e jQuery event.
 *
 * @returns {undefined}
 */
function preventDefault (e) {
    e.preventDefault();
}

globalize('preventDefault', preventDefault);

export default preventDefault;
