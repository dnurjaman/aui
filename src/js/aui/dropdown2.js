import './i18n';
import $ from './jquery';
import template from 'skatejs-template-html';
import * as deprecate from './internal/deprecation';
import * as logger from './internal/log';
import { debounceImmediate } from './debounce';
import { supportsVoiceOver } from './internal/browser';
import Alignment from './internal/alignment';
import CustomEvent from './polyfills/custom-event';
import keyCode from './key-code';
import layer from './layer';
import state from './internal/state';
import skate from 'skatejs';
import skateType from 'skatejs-types';
import './spin';

function isChecked(el) {
    return $(el).is('.checked, .aui-dropdown2-checked, [aria-checked="true"]');
}

function getTrigger(control) {
    return $('[aria-controls="' + control.id + '"]')[0];
}

function doIfTrigger(triggerable, callback) {
    const trigger = getTrigger(triggerable);

    if (trigger) {
        callback(trigger);
    }
}

function setDropdownTriggerActiveState(trigger, isActive) {
    const $trigger = $(trigger);

    if (isActive) {
        $trigger.attr('aria-expanded', 'true');
        $trigger.addClass('active aui-dropdown2-active');
    } else {
        $trigger.attr('aria-expanded', 'false');
        $trigger.removeClass('active aui-dropdown2-active');
    }
}

// LOADING STATES
const UNLOADED = 'unloaded';
const LOADING = 'loading';
const ERROR = 'error';
const SUCCESS = 'success';


// ASYNC DROPDOWN FUNCTIONS

function makeAsyncDropdownContents (json) {
    const dropdownContents = json.map(function makeSection (sectionData) {
        const sectionItemsHtml = sectionData.items.map(function makeSectionItem (itemData) {
            function makeBooleanAttribute (attr) {
                return itemData[attr] ? `${attr} ="true"` : '';
            }

            function makeAttribute (attr) {
                return itemData[attr] ? `${attr}="${itemData[attr]}"` : '';
            }

            const tagName = 'aui-item-' + itemData.type;
            const itemHtml = `
                <${tagName} ${makeAttribute('for')} ${makeAttribute('href')} ${makeBooleanAttribute('interactive')}
                    ${makeBooleanAttribute('checked')} ${makeBooleanAttribute('disabled')} ${makeBooleanAttribute('hidden')}>
                    ${itemData.content}
                </${tagName}>`;

            return itemHtml;
        }).join('');

        const sectionAttributes = sectionData.label ? `label="${sectionData.label}"` : '';
        const sectionHtml = `
            <aui-section ${sectionAttributes}>
                ${sectionItemsHtml}
            </aui-section>`;

        return sectionHtml;
    }).join('\n');

    return dropdownContents;
}

function setDropdownContents (dropdown, json) {
    state(dropdown).set('loading-state', SUCCESS);
    template.wrap(dropdown).innerHTML = makeAsyncDropdownContents(json);
    skate.init(dropdown);
}

function setDropdownErrorState (dropdown) {
    state(dropdown).set('loading-state', ERROR);
    state(dropdown).set('hasErrorBeenShown', dropdown.isVisible());
    template.wrap(dropdown).innerHTML = `
        <div class="aui-message aui-message-error aui-dropdown-error">
            <p>${AJS.I18n.getText('aui.dropdown.async.error')}</p>
        </div>
    `;
}

function setDropdownLoadingState (dropdown) {
    state(dropdown).set('loading-state', LOADING);
    state(dropdown).set('hasErrorBeenShown', false);

    doIfTrigger(dropdown, function (trigger) {
        trigger.setAttribute('aria-busy', 'true');
    });

    template.wrap(dropdown).innerHTML = `
        <div class="aui-dropdown-loading">
            <span class="spinner"></span> ${AJS.I18n.getText('aui.dropdown.async.loading')}
        </div>
    `;
    $(dropdown).find('.spinner').spin();
}

function setDropdownLoaded (dropdown) {
    doIfTrigger(dropdown, function (trigger) {
        trigger.setAttribute('aria-busy', 'false');
    });
}

function loadContentsIfAsync (dropdown) {
    if (!dropdown.src || state(dropdown).get('loading-state') === LOADING) {
        return;
    }

    setDropdownLoadingState(dropdown);

    $.ajax(dropdown.src)
        .done(function (json, status, xhr) {
            const isValidStatus = xhr.status === 200;
            if (isValidStatus) {
                setDropdownContents(dropdown, json);
            } else {
                setDropdownErrorState(dropdown);
            }
        })
        .fail(function () {
            setDropdownErrorState(dropdown);
        })
        .always(function () {
            setDropdownLoaded(dropdown);
        });
}

function loadContentWhenMouseEnterTrigger(dropdown) {
    const isDropdownUnloaded = state(dropdown).get('loading-state') === UNLOADED;
    const hasCurrentErrorBeenShown = state(dropdown).get('hasErrorBeenShown');
    if (isDropdownUnloaded || hasCurrentErrorBeenShown && !dropdown.isVisible()) {
        loadContentsIfAsync(dropdown);
    }
}

function loadContentWhenMenuShown(dropdown) {
    const isDropdownUnloaded = state(dropdown).get('loading-state') === UNLOADED;
    const hasCurrentErrorBeenShown = state(dropdown).get('hasErrorBeenShown');
    if (isDropdownUnloaded || hasCurrentErrorBeenShown) {
        loadContentsIfAsync(dropdown);
    }
    if (state(dropdown).get('loading-state') === ERROR) {
        state(dropdown).set('hasErrorBeenShown', true);
    }
}


// The dropdown's trigger
// ----------------------

function getDropdownId (trigger) {
    return trigger.getAttribute('aria-controls') || trigger.getAttribute('aria-owns');
}

function getDropdownForTrigger (trigger) {
    return document.getElementById(getDropdownId(trigger));
}

function triggerReady (trigger) {
    let dropdownID = getDropdownId(trigger);

    if (trigger.hasAttribute('aria-owns')) {
        trigger.removeAttribute('aria-owns');
        trigger.setAttribute('aria-controls', dropdownID);
    }

    if (!dropdownID) {
        logger.error('Dropdown triggers need either a "aria-owns" or "aria-controls" attribute');
    }

    trigger.setAttribute('aria-haspopup', true);
    trigger.setAttribute('aria-expanded', false);
    trigger.setAttribute('href', '#');

    function handleIt(e) {
        let trigger = e.currentTarget;
        e.preventDefault();

        if (!trigger.isEnabled()) {
            return;
        }

        let dropdown = getDropdownForTrigger(trigger);
        dropdown.toggle();
        dropdown.isSubmenu = trigger.hasSubmenu();

        return dropdown;
    }

    function handleMouseEnter(e) {
        let trigger = e.currentTarget;
        e.preventDefault();

        if (!trigger.isEnabled()) {
            return;
        }

        let dropdown = getDropdownForTrigger(trigger);
        loadContentWhenMouseEnterTrigger(dropdown);

        if (trigger.hasSubmenu()) {
            dropdown.show();
            dropdown.isSubmenu = trigger.hasSubmenu();
        }

        return dropdown;
    }

    function handleKeydown(e) {
        let trigger = e.currentTarget;
        let normalInvoke = (e.keyCode === keyCode.ENTER || e.keyCode === keyCode.SPACE);
        let submenuInvoke = (e.keyCode === keyCode.RIGHT && trigger.hasSubmenu());
        let rootMenuInvoke = ((e.keyCode === keyCode.UP || e.keyCode === keyCode.DOWN) && !trigger.hasSubmenu());

        if (normalInvoke || submenuInvoke || rootMenuInvoke) {
            const dropdown = handleIt(e);

            if (dropdown) {
                dropdown.focusItem(0);
            }
        }
    }
    $(trigger)
        .on('aui-button-invoke', handleIt)
        .on('click', handleIt)
        .on('keydown', handleKeydown)
        .on('mouseenter', handleMouseEnter)
    ;
}

const triggerPrototype = {
    disable: function () {
        this.setAttribute('aria-disabled', 'true');
    },
    enable: function () {
        this.setAttribute('aria-disabled', 'false');
    },
    isEnabled: function () {
        return this.getAttribute('aria-disabled') !== 'true';
    },
    hasSubmenu: function () {
        const triggerClasses = (this.className || '').split(/\s+/);
        return triggerClasses.indexOf('aui-dropdown2-sub-trigger') !== -1;
    }
};

skate('aui-dropdown2-trigger', {
    type: skateType.classname,
    ready: triggerReady,
    prototype: triggerPrototype
});

//To remove at a later date. Some dropdown triggers initialise lazily, so we need to listen for mousedown
//and synchronously init before the click event is fired.
//TODO: delete in AUI 6.0.0, see AUI-2868
function bindLazyTriggerInitialisation() {
    $(document).on('mousedown', '.aui-dropdown2-trigger', function () {
        const isElementSkated = this.hasAttribute('resolved');
        if (!isElementSkated) {
            skate.init(this);
            const lazyDeprecate = deprecate.getMessageLogger('Dropdown2 lazy initialisation', {
                removeInVersion: '6.0.0',
                alternativeName: 'initialisation on DOM insertion',
                sinceVersion: '5.8.0',
                extraInfo: 'Dropdown2 triggers should have all necessary attributes on DOM insertion',
                deprecationType: 'JS'
            });
            lazyDeprecate();
        }
    });
}

bindLazyTriggerInitialisation();

skate('aui-dropdown2-sub-trigger', {
    type: skateType.classname,
    ready: function (dropdown) {
        dropdown.className += ' aui-dropdown2-trigger';
        //We need to skate.init every time a skated class is added to dropdown – see http://jsbin.com/juxoyo/1/edit
        skate.init(dropdown);
    }
});


// Dropdown trigger groups
// -----------------------

$(document).on('mouseenter', '.aui-dropdown2-trigger-group a, .aui-dropdown2-trigger-group button', function (e) {
    const $item = $(e.currentTarget);

    if ($item.is('.aui-dropdown2-active')) {
        return; // No point doing anything if we're hovering over the already-active item trigger.
    }

    if ($item.closest('.aui-dropdown2').size()) {
        return; // We don't want to deal with dropdown items, just the potential triggers in the group.
    }

    const $triggerGroup = $item.closest('.aui-dropdown2-trigger-group');

    const $groupActiveTrigger = $triggerGroup.find('.aui-dropdown2-active');
    if ($groupActiveTrigger.size() && $item.is('.aui-dropdown2-trigger')) {
        $groupActiveTrigger.blur(); // Remove focus from the previously opened menu.
        $item.trigger('aui-button-invoke'); // Open this trigger's menu.
        e.preventDefault();
    }

    const $groupFocusedTrigger = $triggerGroup.find(':focus');
    if ($groupFocusedTrigger.size() && $item.is('.aui-dropdown2-trigger')) {
        $groupFocusedTrigger.blur();
    }
});


// Dropdown items
// --------------

function getDropdownItems (dropdown, filter) {
    return $(dropdown)
        .find([
            // Legacy markup.
            '> ul > li',
            '> .aui-dropdown2-section > ul > li',
            // Accessible markup.
            '> div[role] > .aui-dropdown2-section > div[role="group"] > ul[role] > li[role]',
            // Web component.
            'aui-item-link',
            'aui-item-checkbox',
            'aui-item-radio'
        ].join(', '))
        .filter(filter)
        .children('a, button, [role="checkbox"], [role="menuitemcheckbox"], [role="radio"], [role="menuitemradio"]');
}

function getAllDropdownItems (dropdown) {
    return getDropdownItems(dropdown, function () {
        return true;
    });
}

function getVisibleDropdownItems (dropdown) {
    return getDropdownItems(dropdown, function () {
        return this.className.indexOf('hidden') === -1 && !this.hasAttribute('hidden');
    });
}

function amendDropdownItem (item) {
    const $item = $(item);

    $item.attr('tabindex', '-1');

    /**
     * Honouring the documentation.
     * @link https://docs.atlassian.com/aui/latest/docs/dropdown2.html
     */
    if ($item.hasClass('aui-dropdown2-disabled') || $item.parent().hasClass('aui-dropdown2-hidden')) {
        $item.attr('aria-disabled', true);
    }
}

function amendDropdownContent (dropdown) {
    // Add assistive semantics to each dropdown item
    getAllDropdownItems(dropdown).each(function () {
        amendDropdownItem(this);
    });
}

/**
 * Honours behaviour for code written using only the legacy class names.
 * To maintain old behaviour (i.e., remove the 'hidden' class and the item will become un-hidden)
 * whilst allowing our code to only depend on the new classes, we need to
 * keep the state of the DOM in sync with legacy classes.
 *
 * Calling this function will add the new namespaced classes to elements with legacy names.
 * @returns {Function} a function to remove the new namespaced classes, only from the elements they were added to.
 */
function migrateAndSyncLegacyClassNames (dropdown) {
    const $dropdown = $(dropdown);

    // Migrate away from legacy class names
    const $hiddens = $dropdown.find('.hidden').addClass('aui-dropdown2-hidden');
    const $disableds = $dropdown.find('.disabled').addClass('aui-dropdown2-disabled');
    const $interactives = $dropdown.find('.interactive').addClass('aui-dropdown2-interactive');

    return function revertToOriginalMarkup () {
        $hiddens.removeClass('aui-dropdown2-hidden');
        $disableds.removeClass('aui-dropdown2-disabled');
        $interactives.removeClass('aui-dropdown2-interactive');
    };
}


// The Dropdown itself
// -------------------

function setLayerAlignment(dropdown, trigger) {
    const hasSubmenu = trigger && trigger.hasSubmenu && trigger.hasSubmenu();
    const hasSubmenuAlignment = dropdown.getAttribute('data-aui-alignment') === 'submenu auto';

    if (!hasSubmenu && hasSubmenuAlignment) {
        restorePreviousAlignment(dropdown);
    }
    const hasAnyAlignment = dropdown.hasAttribute('data-aui-alignment');

    if (hasSubmenu && !hasSubmenuAlignment) {
        saveCurrentAlignment(dropdown);
        dropdown.setAttribute('data-aui-alignment', 'submenu auto');
        dropdown.setAttribute('data-aui-alignment-static', true);
    } else if (!hasAnyAlignment) {
        dropdown.setAttribute('data-aui-alignment', 'bottom auto');
        dropdown.setAttribute('data-aui-alignment-static', true);
    }

    if (dropdown._auiAlignment) {
        dropdown._auiAlignment.destroy();
    }

    dropdown._auiAlignment = new Alignment(dropdown, trigger);

    dropdown._auiAlignment.enable();
}

function saveCurrentAlignment(dropdown) {
    const $dropdown = $(dropdown);
     if (dropdown.hasAttribute('data-aui-alignment')) {
         $dropdown.data('previous-data-aui-alignment', dropdown.getAttribute('data-aui-alignment'));
     }
     $dropdown.data('had-data-aui-alignment-static', dropdown.hasAttribute('data-aui-alignment-static'));
 }

function restorePreviousAlignment(dropdown) {
    const $dropdown = $(dropdown);

    const previousAlignment = $dropdown.data('previous-data-aui-alignment');
     if (previousAlignment) {
         dropdown.setAttribute('data-aui-alignment', previousAlignment);
     } else {
         dropdown.removeAttribute('data-aui-alignment');
     }
     $dropdown.removeData('previous-data-aui-alignment');

     if (!$dropdown.data('had-data-aui-alignment-static')) {
         dropdown.removeAttribute('data-aui-alignment-static');
     }
     $dropdown.removeData('had-data-aui-alignment-static');
 }

function getDropdownHideLocation(dropdown, trigger) {
    const possibleHome = trigger.getAttribute('data-dropdown2-hide-location');
    return document.getElementById(possibleHome) || dropdown.parentNode;
}

var keyboardClose = false;
function keyboardCloseDetected () {
    keyboardClose = true;
}

function wasProbablyClosedViaKeyboard () {
    const result = (keyboardClose === true);
    keyboardClose = false;
    return result;
}

function bindDropdownBehaviourToLayer(dropdown) {
    layer(dropdown);


    dropdown.addEventListener('aui-layer-show', function () {
        $(dropdown).trigger('aui-dropdown2-show');

        dropdown._syncClasses = migrateAndSyncLegacyClassNames(dropdown);

        amendDropdownContent(dropdown);

        doIfTrigger(dropdown, function (trigger) {
            setDropdownTriggerActiveState(trigger, true);
            dropdown._returnTo = getDropdownHideLocation(dropdown, trigger);
        });
    });

    dropdown.addEventListener('aui-layer-hide', function () {
        $(dropdown).trigger('aui-dropdown2-hide');

        if (dropdown._syncClasses) {
            dropdown._syncClasses();
            delete dropdown._syncClasses;
        }

        if (dropdown._auiAlignment) {
            dropdown._auiAlignment.disable();
            dropdown._auiAlignment.destroy();
        }

        if (dropdown._returnTo) {
            if (dropdown.parentNode && dropdown.parentNode !== dropdown._returnTo) {
                dropdown.parentNode.removeChild(dropdown);
            }
            dropdown._returnTo.appendChild(dropdown);
        }

        getVisibleDropdownItems(dropdown).removeClass('active aui-dropdown2-active');

        doIfTrigger(dropdown, function (trigger) {
            if (wasProbablyClosedViaKeyboard()) {
                trigger.focus();
                setDropdownTriggerActiveState(trigger, trigger.hasSubmenu && trigger.hasSubmenu());
            } else {
                setDropdownTriggerActiveState(trigger, false);
            }
        });

        // Gets set by submenu trigger invocation. Bad coupling point?
        delete dropdown.isSubmenu;
    });
}

function bindItemInteractionBehaviourToDropdown (dropdown) {
    const $dropdown = $(dropdown);

    $dropdown.on('keydown', function (e) {
        if (e.keyCode === keyCode.DOWN) {
            dropdown.focusNext();
            e.preventDefault();
        } else if (e.keyCode === keyCode.UP) {
            dropdown.focusPrevious();
            e.preventDefault();
        } else if (e.keyCode === keyCode.LEFT) {
            if (dropdown.isSubmenu) {
                keyboardCloseDetected();
                dropdown.hide();
                e.preventDefault();
            }
        } else if (e.keyCode === keyCode.ESCAPE) {
            // The closing will be handled by the LayerManager!
            keyboardCloseDetected();
        } else if (e.keyCode === keyCode.TAB) {
            keyboardCloseDetected();
            dropdown.hide();
        }
    });

    // close the menu when activating elements which aren't "interactive"
    $dropdown.on('click keydown', 'a, button, [role="menuitem"], [role="menuitemcheckbox"], [role="checkbox"], [role="menuitemradio"], [role="radio"]', function (e) {
        if (e.type === 'click' || e.keyCode === keyCode.ENTER || e.keyCode === keyCode.SPACE) {
            var $item = $(e.currentTarget);

            if ($item.attr('aria-disabled') === 'true') {
                e.preventDefault();
            }

            if (!e.isDefaultPrevented() && !$item.is('.aui-dropdown2-interactive')) {
                var theMenu = dropdown;
                do {
                    var dd = layer(theMenu);
                    theMenu = layer(theMenu).below();
                    if (dd.$el.is('.aui-dropdown2')) {
                        dd.hide();
                    }
                } while (theMenu);
            }
        }
    });

    // close a submenus when the mouse moves over items other than its trigger
    $dropdown.on('mouseenter', 'a, button, [role="menuitem"], [role="menuitemcheckbox"], [role="checkbox"], [role="menuitemradio"], [role="radio"]', function (e) {
        const item = e.currentTarget;
        const hasSubmenu = item.hasSubmenu && item.hasSubmenu();

        if (!e.isDefaultPrevented() && !hasSubmenu) {
            const maybeALayer = layer(dropdown).above();

            if (maybeALayer) {
                layer(maybeALayer).hide();
            }
        }
    });
}

$(window).on('resize', debounceImmediate(function () {
    $('.aui-dropdown2').each(function (index, dropdown) {
        skate.init(dropdown);
        if (dropdown.isVisible()){
            dropdown.hide();
        }
    });
}, 1000));

// Dropdowns
// ---------

function dropdownCreated (dropdown) {
    const $dropdown = $(dropdown);

    $dropdown.addClass('aui-dropdown2');

    // swap the inner div to presentation as application is only needed for Windows
    if (supportsVoiceOver()) {
        $dropdown.find('> div[role="application"]').attr('role', 'presentation');
    }

    if (dropdown.hasAttribute('data-container')) {
        $dropdown.attr('data-aui-alignment-container', $dropdown.attr('data-container'));
        $dropdown.removeAttr('data-container');
    }

    bindDropdownBehaviourToLayer(dropdown);
    bindItemInteractionBehaviourToDropdown(dropdown);
    dropdown.hide();

    $dropdown.delegate('.aui-dropdown2-checkbox:not(.disabled):not(.aui-dropdown2-disabled)', 'click keydown', function (e) {
        if (e.type === 'click' || e.keyCode === keyCode.ENTER || e.keyCode === keyCode.SPACE) {
            let checkbox = this;
            if (e.isDefaultPrevented()) {
                return;
            }
            if (checkbox.isInteractive()) {
                e.preventDefault();
            }
            if (checkbox.isEnabled()) {
                // toggle the checked state
                if (checkbox.isChecked()) {
                    checkbox.uncheck();
                } else {
                    checkbox.check();
                }
            }
        }
    });

    $dropdown.delegate('.aui-dropdown2-radio:not(.checked):not(.aui-dropdown2-checked):not(.disabled):not(.aui-dropdown2-disabled)', 'click keydown', function (e) {
        if (e.type === 'click' || e.keyCode === keyCode.ENTER || e.keyCode === keyCode.SPACE) {
            let radio = this;
            skate.init(this);
            if (e.isDefaultPrevented()) {
                return;
            }
            if (radio.isInteractive()) {
                e.preventDefault();
            }

            let $radio = $(this);
            if (this.isEnabled() && this.isChecked() === false) {
                // toggle the checked state
                $radio.closest('ul,[role=group]').find('.aui-dropdown2-checked').not(this).each(function () {
                    this.uncheck();
                });
                radio.check();
            }
        }
    });
}

const dropdownPrototype = {
    /**
     * Toggles the visibility of the dropdown menu
     */
    toggle: function () {
        if (this.isVisible()) {
            this.hide();
        } else {
            this.show();
        }
    },

    /**
     * Explicitly shows the menu
     *
     * @returns {HTMLElement}
     */
    show: function () {
        layer(this).show();

        const dropdown = this;
        doIfTrigger(dropdown, function (trigger) {
            setLayerAlignment(dropdown, trigger);
        });

        return this;
    },

    /**
     * Explicitly hides the menu
     *
     * @returns {HTMLElement}
     */
    hide: function () {
        layer(this).hide();
        return this;
    },

    /**
     * Shifts explicit focus to the next available item in the menu
     *
     * @returns {undefined}
     */
    focusNext: function () {
        const $items = getVisibleDropdownItems(this);
        const selected = document.activeElement;
        let idx;

        if ($items.last()[0] !== selected) {
            idx = $items.toArray().indexOf(selected);
            this.focusItem($items.get(idx + 1));
        }
    },

    /**
     * Shifts explicit focus to the previous available item in the menu
     *
     * @returns {undefined}
     */
    focusPrevious: function () {
        const $items = getVisibleDropdownItems(this);
        const selected = document.activeElement;
        let idx;

        if ($items.first()[0] !== selected) {
            idx = $items.toArray().indexOf(selected);
            this.focusItem($items.get(idx- 1));
        }
    },

    /**
     * Shifts explicit focus to the menu item matching the index param
     */
    focusItem: function (item) {
        const $items = getVisibleDropdownItems(this);
        let $item;
        if (typeof item === 'number') {
            item = $items.get(item);
        }
        $item = $(item);
        $item.focus();
        $items.removeClass('active aui-dropdown2-active');
        $item.addClass('active aui-dropdown2-active');
    },

    /**
     * Checks whether or not the menu is currently displayed
     *
     * @returns {Boolean}
     */
    isVisible: function () {
        return layer(this).isVisible();
    }
};

// Web component API for dropdowns
// -------------------------------
const disabledPropertyHandler = skate.properties.boolean({
    attribute: true,
    set (el, {newValue, oldValue}) {
        const $a = $(el.children[0]);
        if (newValue) {
            $a.attr('aria-disabled', 'true');
            $a.addClass('aui-dropdown2-disabled');
        } else if (oldValue !== undefined) {
            $a.attr('aria-disabled', 'false');
            $a.removeClass('aui-dropdown2-disabled');
        }
    }
});

const interactivePropertyHandler = skate.properties.boolean({
    attribute: true,
    set (el, {newValue: value}) {
        const $a = $(el.children[0]);
        if (value) {
            $a.addClass('aui-dropdown2-interactive');
        } else {
            $a.removeClass('aui-dropdown2-interactive');
        }
    }
});

const checkedPropertyHandler = skate.properties.boolean({
    attribute: true,
    set (el, {newValue: value}) {
        const $a = $(el.children[0]);
        if (value) {
            $a.addClass('checked aui-dropdown2-checked');
            $a.attr('aria-checked', 'true');
            el.dispatchEvent(new CustomEvent('change', {bubbles: true}));
        } else {
            $a.removeClass('checked aui-dropdown2-checked');
            $a.attr('aria-checked', 'false');
            el.dispatchEvent(new CustomEvent('change', {bubbles: true}));
        }
    }
});

const hiddenPropertyHandler = skate.properties.boolean({
    attribute: true,
    set (el, {newValue, oldValue}) {
        const $a = $(el.children[0]);
        if (newValue) {
            $a.attr('aria-disabled', 'true');
            $a.addClass('aui-dropdown2-disabled');
        } else if (oldValue !== undefined) {
            $a.attr('aria-disabled', 'false');
            $a.removeClass('aui-dropdown2-disabled');
        } else {
            el.removeAttribute('hidden');
        }
    }
});

skate('aui-item-link', {
    created: function (el) {
        template(
            '<a role="menuitem" tabindex="-1"><content></content></a>'
        )(el);
    },
    properties: {
        disabled: disabledPropertyHandler,
        interactive: interactivePropertyHandler,
        hidden: hiddenPropertyHandler,
        href: {
            attribute: true,
            set (el, {newValue: value}) {
                let a = el.children[0];
                if (value) {
                    a.href = value;
                } else {
                    a.removeAttribute('href');
                }
            }
        },
        for: {
            attribute: true,
            set (el, {newValue: value}) {
                let $anchor = $(el.children[0]);
                if (value) {
                    $anchor.attr('aria-controls', value);
                    $anchor.addClass('aui-dropdown2-sub-trigger');
                    //We need to skate.init every time a skated class is added to dropdown – see http://jsbin.com/juxoyo/1/edit
                    skate.init($anchor[0]);
                } else {
                    $anchor.removeAttr('aria-controls');
                    $anchor.removeClass('aui-dropdown2-sub-trigger');
                }
            }
        }
    }
});

skate('aui-item-checkbox', {
    created: function (el) {
        template(
            '<span role="checkbox" class="aui-dropdown2-checkbox" tabindex="-1"><content></content></span>'
        )(el);
        //We need to skate.init every time a skated class is added to dropdown – see http://jsbin.com/juxoyo/1/edit
        skate.init(el);
    },
    properties: {
        disabled: disabledPropertyHandler,
        interactive: interactivePropertyHandler,
        checked: checkedPropertyHandler,
        hidden: hiddenPropertyHandler
    }
});

skate('aui-item-radio', {
    created: function (el) {
        template(
            '<span role="radio" class="aui-dropdown2-radio" tabindex="-1"><content></content></span>'
        )(el);

        //We need to skate.init every time a skated class is added to dropdown – see http://jsbin.com/juxoyo/1/edit
        skate.init(el);
    },
    properties: {
        disabled: disabledPropertyHandler,
        interactive: interactivePropertyHandler,
        checked: checkedPropertyHandler,
        hidden: hiddenPropertyHandler
    }
});

skate('aui-section', {
    created: function (el) {
        template(`
            <strong aria-role="presentation" class="aui-dropdown2-heading"></strong>
            <div role="group">
                <content></content>
            </div>`
        )(el);
    },
    properties: {
        label: {
            attribute: true,
            set (el, {newValue: value}) {
                let headingElement = el.children[0];
                let groupElement = el.children[1];
                headingElement.textContent = value;
                groupElement.setAttribute('aria-label', value);
            }
        }
    },
    ready: function (el) {
        el.className += ' aui-dropdown2-section';
        el.setAttribute('role', 'presentation');
    }
});

skate('aui-dropdown-menu', {
    created: function (el) {
        template(`
            <div role="application">
                <content></content>
            </div>
        `)(el);
    },
    ready: function (el) {
        el.setAttribute('role', 'menu');
        el.className = 'aui-dropdown2 aui-style-default aui-layer';

        state(el).set('loading-state', UNLOADED);
        //We need to skate.init every time a skated class is added to dropdown – see http://jsbin.com/juxoyo/1/edit
        skate.init(el);
    },
    properties: {
        src: {
            attribute: true
        }
    },
    prototype: dropdownPrototype,
    events: {
        'aui-layer-show': function () {
            loadContentWhenMenuShown(this);
        }
    }
});

// Legacy dropdown inits
// ---------------------

skate('aui-dropdown2', {
    type: skateType.classname,
    ready: dropdownCreated,
    prototype: dropdownPrototype
});

skate('data-aui-dropdown2', {
    type: skateType.attribute,
    ready: dropdownCreated,
    prototype: dropdownPrototype
});


// Checkboxes and radios
// ---------------------

skate('aui-dropdown2-checkbox', {
    type: skateType.classname,
    ready: function (el) {
        const checked = isChecked(el);
        if (checked) {
            $(el).addClass('checked aui-dropdown2-checked');
        }
        el.setAttribute('aria-checked', checked);
        el.setAttribute('tabindex', '0');

        // swap from menuitemcheckbox to just plain checkbox for VoiceOver
        if (supportsVoiceOver()) {
            el.setAttribute('role','checkbox');
        }

        $(el).on('click keydown', function(e) {
            if (e.type === 'click' || e.keyCode === keyCode.ENTER || e.keyCode === keyCode.SPACE) {
                if (e.isDefaultPrevented()) {
                    return;
                }

                if (this.isInteractive()) {
                    e.preventDefault();
                }

                if (this.isEnabled()) {
                    // toggle the checked state
                    if (this.isChecked()) {
                        this.uncheck();
                    } else {
                        this.check();
                    }
                }
            }
        });
    },
    prototype: {
        isEnabled: function () {
            return !(this.getAttribute('aria-disabled') !== null && this.getAttribute('aria-disabled') === 'true');
        },
        isChecked: function () {
            return this.getAttribute('aria-checked') !== null && this.getAttribute('aria-checked') === 'true';
        },
        isInteractive: function () {
            return $(this).hasClass('aui-dropdown2-interactive');
        },
        uncheck: function () {
            if (this.parentNode.tagName.toLowerCase() === 'aui-item-checkbox') {
                this.parentNode.removeAttribute('checked');
            }
            this.setAttribute('aria-checked', 'false');
            $(this).removeClass('checked aui-dropdown2-checked');
            $(this).trigger('aui-dropdown2-item-uncheck');
        },
        check: function () {
            if (this.parentNode.tagName.toLowerCase() === 'aui-item-checkbox') {
                this.parentNode.setAttribute('checked', '');
            }
            this.setAttribute('aria-checked', 'true');
            $(this).addClass('checked aui-dropdown2-checked');
            $(this).trigger('aui-dropdown2-item-check');
        }
    }
});

skate('aui-dropdown2-radio', {
    type: skateType.classname,
    ready: function (el) {
        // add a dash of ARIA
        const checked = isChecked(el);
        if (checked) {
            $(el).addClass('checked aui-dropdown2-checked');
        }
        el.setAttribute('aria-checked', checked);
        el.setAttribute('tabindex', '0');

        // swap from menuitemradio to just plain radio for VoiceOver
        if (supportsVoiceOver()) {
            el.setAttribute('role','radio');
        }

        $(el).on('click keydown', function (e){
            if (e.type === 'click' || e.keyCode === keyCode.ENTER || e.keyCode === keyCode.SPACE) {
                if (e.isDefaultPrevented()) {
                    return;
                }

                if (this.isInteractive()) {
                    e.preventDefault();
                }
                const $radio = $(this);

                if (this.isEnabled() && this.isChecked() === false) {
                    // toggle the checked state
                    $radio.closest('ul,[role=group]').find('.aui-dropdown2-checked').not(this).each(function(){
                        this.uncheck();
                    });
                    this.check();
                }
            }
        });
    },
    prototype: {
        isEnabled: function () {
            return !(this.getAttribute('aria-disabled') !== null && this.getAttribute('aria-disabled') === 'true');
        },
        isChecked: function () {
            return this.getAttribute('aria-checked') !== null && this.getAttribute('aria-checked') === 'true';
        },
        isInteractive: function () {
            return $(this).hasClass('aui-dropdown2-interactive');
        },
        uncheck: function () {
            if (this.parentNode.tagName.toLowerCase() === 'aui-item-radio') {
                this.parentNode.removeAttribute('checked');
            }
            this.setAttribute('aria-checked', 'false');
            $(this).removeClass('checked aui-dropdown2-checked');
            $(this).trigger('aui-dropdown2-item-uncheck');
        },

        check: function () {
            if (this.parentNode.tagName.toLowerCase() === 'aui-item-radio') {
                this.parentNode.setAttribute('checked', '');
            }
            this.setAttribute('aria-checked', 'true');
            $(this).addClass('checked aui-dropdown2-checked');
            $(this).trigger('aui-dropdown2-item-check');
        }
    }
});
