'use strict';

import $ from './jquery';
import amdify from './internal/amdify';
import skate from 'skatejs';
import skateType from 'skatejs-types';

function isNestedAnchor(trigger, target) {
    var $closestAnchor = $(target).closest('a[href]', trigger);
    return !!$closestAnchor.length && $closestAnchor[0] !== trigger;
}

function findControlled (trigger) {
    return document.getElementById(trigger.getAttribute('aria-controls'));
}

function triggerMessage (trigger, e) {
    if (trigger.isEnabled()) {
        var component = findControlled(trigger);
        if (component && component.message) {
            component.message(e);
        }
    }
}

skate('data-aui-trigger', {
    type: skateType.attribute,
    events: {
        click: function (e) {
            if (!isNestedAnchor(this, e.target)) {
                triggerMessage(this, e);
                e.preventDefault();
            }
        },
        mouseenter: function (e) {
            triggerMessage(this, e);
        },
        mouseleave: function (e) {
            triggerMessage(this, e);
        },
        focus: function (e) {
            triggerMessage(this, e);
        },
        blur: function (e) {
            triggerMessage(this, e);
        }
    },
    prototype: {
        disable: function () {
            this.setAttribute('aria-disabled', 'true');
        },
        enable: function () {
            this.setAttribute('aria-disabled', 'false');
        },
        isEnabled: function () {
            return this.getAttribute('aria-disabled') !== 'true';
        }
    }
});

amdify('aui/trigger');
