'use strict';

import component from './internal/component';
import enforce from './internal/enforcer';
import properties from './internal/properties';

const SIZE_ATTRIBUTE_ENUM = {
    attribute: 'size',
    values: ['xsmall', 'small', 'medium', 'large', 'xlarge', 'xxlarge', 'xxxlarge'],
    missingDefault: 'medium',
    invalidDefault: 'medium'
};
const AUI_AVATAR_DEFAULT_TYPE = 'user';

function getImg(element) {
    return element.querySelector('img');
}

function setAttributeOnInnerImage (element, changeData) {
    if (changeData.newValue === undefined) {
        getImg(element).removeAttribute(changeData.name);
        return;
    }

    getImg(element).setAttribute(changeData.name, changeData.newValue);
}

export default component('avatar', {
    render: function(element) {
        element.innerHTML =
            `<aui-avatar-inner>
               <img class="aui-avatar-img">
            </aui-avatar-inner>`;
    },
    attached: function (element) {
        enforce(element).attributeExists('src');
        enforce(element).attributeExists('alt');
    },
    properties: {
        size: properties.enum(SIZE_ATTRIBUTE_ENUM)({
            attribute: true
        }),
        type: {
            attribute: true,
            default: AUI_AVATAR_DEFAULT_TYPE
        },
        src: {
            attribute: true,
            set: setAttributeOnInnerImage
        },
        alt: {
            attribute: true,
            set: setAttributeOnInnerImage
        }
    }
});
