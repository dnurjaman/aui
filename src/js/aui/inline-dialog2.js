'use strict';

import $ from './jquery';
import Alignment from './internal/alignment';
import amdify from './internal/amdify';
import attributes from './internal/attributes';
import component from './internal/component';
import enforce from './internal/enforcer';
import layer from './layer';
import properties from './internal/properties';
import render from 'skatejs-dom-diff/lib/render';
import skate from 'skatejs';
import namedSlots from 'skatejs-named-slots';
import state from './internal/state';

const DEFAULT_HOVEROUT_DELAY = 1000;

function getTrigger (element) {
    return document.querySelector('[aria-controls="' + element.id + '"]');
}

function doIfTrigger (element, callback) {
    const trigger = getTrigger(element);
    if (trigger) {
        callback(trigger);
    }
}

function initAlignment (element, trigger) {
    if (!element._auiAlignment) {
        element._auiAlignment = new Alignment(element, trigger);
    }
}

function enableAlignment (element, trigger) {
    initAlignment(element, trigger);
    element._auiAlignment.enable();
}

function disableAlignment (element, trigger) {
    initAlignment(element, trigger);
    element._auiAlignment.disable();
}

function handleMessage (element, message) {
    const messageTypeMap = {
        toggle: ['click'],
        hover: ['mouseenter', 'mouseleave', 'focus', 'blur']
    };
    const messageList = messageTypeMap[element.respondsTo];
    if (messageList && messageList.indexOf(message.type) > -1) {
        messageHandler[message.type](element, message);
    }
}

const messageHandler = {
    click: function (element) {
        if (element.open) {
            if (!layer(element).isPersistent()) {
                element.open = false;
            }
        } else {
            element.open = true;
        }
    },

    mouseenter: function (element) {
        if (!element.open) {
            element.open = true;
        }

        if (element._clearMouseleaveTimeout) {
            element._clearMouseleaveTimeout();
        }
    },

    mouseleave: function (element) {
        if (layer(element).isPersistent() || !element.open) {
            return;
        }

        if (element._clearMouseleaveTimeout) {
            element._clearMouseleaveTimeout();
        }

        const timeout = setTimeout(function () {
            if (!state(element).get('mouse-inside')) {
                element.open = false;
            }
        }, DEFAULT_HOVEROUT_DELAY);

        element._clearMouseleaveTimeout = function () {
            clearTimeout(timeout);
            element._clearMouseleaveTimeout = null;
        };
    },

    focus: function (element) {
        if (!element.open) {
            element.open = true;
        }
    },

    blur: function (element) {
        if (!layer(element).isPersistent() && element.open) {
            element.open = false;
        }
    }
};

function onMouseEnter(e) {
    const element = e.target;
    state(element).set('mouse-inside', true);
    element.message({
        type: 'mouseenter'
    });
}

function onMouseLeave(e) {
    const element = e.target;
    state(element).set('mouse-inside', false);
    element.message({
        type: 'mouseleave'
    });
}

function rebindMouseEvents(el) {
    state(el).set('mouse-inside', undefined);
    el.removeEventListener('mouseenter', onMouseEnter);
    el.removeEventListener('mouseleave', onMouseLeave);

    if (el.respondsTo === 'hover') {
        state(el).set('mouse-inside', false);
        el.addEventListener('mouseenter', onMouseEnter);
        el.addEventListener('mouseleave', onMouseLeave);
    }
}

function showInlineDialog(element) {
    if (state(element).get('is-processing-show')) {
        // We don't want to re-enter show, as the attached
        // handler will get called when the element is appended to the body,
        // causing a double show.
        return;
    }

    state(element).set('is-processing-show', true);
    layer(element).show();

    if (layer(element).isVisible()) {
        doIfTrigger(element, function (trigger) {
            enableAlignment(element, trigger);
            trigger.setAttribute('aria-expanded', 'true');
        });
    }

    state(element).set('is-processing-show', false);
}

function hideInlineDialog(element) {
    layer(element).hide();
    if (!layer(element).isVisible()) {
        doIfTrigger(element, function (trigger) {
            disableAlignment(element, trigger);
            trigger.setAttribute('aria-expanded', 'false');
        });
    }
}

function reflectOpenness(element, shouldBeOpen) {
    if (!isInBody(element)) {
        // If we show when the element is not in the body, layer manager might move it to the body when we show it
        return;
    }

    if (shouldBeOpen) {
        showInlineDialog(element);
    } else {
        hideInlineDialog(element);
    }
}

function isInBody (element) {
    return document.body.contains(element);
}

const RESPONDS_TO_ATTRIBUTE_ENUM = {
    values: ['toggle', 'hover'],
    missingDefault: 'toggle',
    invalidDefault: 'toggle'
};

export default amdify('aui/inline-dialog2', component('inline-dialog', {
    prototype: {
        message (msg) {
            handleMessage(this, msg);
            return this;
        }
    },

    created (elem) {
        $(elem).addClass('aui-layer');
        doIfTrigger(elem, function (trigger) {
            trigger.setAttribute('aria-expanded', elem.open);
            trigger.setAttribute('aria-haspopup', 'true');
        });
    },

    properties: {
        content: namedSlots.slot({
            default: true,
            set: skate.render
        }),

        open: skate.properties.boolean({
            attribute: true,
            /**
             * Opens or closes the inline dialog, returning whether the dialog is
             * open or closed as a result (since event handlers can prevent either
             * action).
             *
             * You should check the value of open after setting this
             * value since the before show/hide events may have prevented it.
             */
            set (element, changeData) {
                // TODO AUI-3726 Revisit double calls to canceled event handlers.
                // Explicitly calling reflectOpenness(…) in this setter means
                // that in native we'll get two sync calls to reflectOpenness(…)
                // and in polyfill one sync (here) and one async (attr change
                // handler). The latter of the two calls, for both cases, will
                // usually be a noop (except when show/hide events are cancelled).
                reflectOpenness(element, changeData.newValue);
            },
            get (element) {
                // We don't want skate to `get` a false because it has no visible
                // layer when it's not in the DOM, and then remove the linked attribute
                if (isInBody(element)) {
                    return layer(element).isVisible();
                } else {
                    return element.hasAttribute('open');
                }
            }
        }),
        persistent: skate.properties.boolean({
            attribute: true
        }),
        respondsTo: properties.enum(RESPONDS_TO_ATTRIBUTE_ENUM)({
            attribute: true,
            set (element, changeData) {
                if (changeData.oldValue !== changeData.newValue) {
                    rebindMouseEvents(element);
                }
            }
        })
    },

    attribute (element, changeData) {
        if (changeData.name === 'aria-hidden') {
            // Whenever layer manager hides us, we need to sync the open attribute.
            attributes.setBooleanAttribute(element, 'open', changeData.newValue === 'false');
        }
    },

    attached (element) {
        enforce(element).attributeExists('id');
        reflectOpenness(element, element.hasAttribute('open'));
        rebindMouseEvents(element);
    },

    detached (element) {
        if (element._auiAlignment) {
            element._auiAlignment.destroy();
        }
    },

    render: render(function (elem, React) {
        return (
            <div class="aui-content aui-inline-dialog-contents">
                {elem.content}
            </div>
        );
    })
}));
