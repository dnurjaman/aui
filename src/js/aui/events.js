'use strict';

import $ from './jquery';
import * as deprecate from './internal/deprecation';
import createElement from './create-element';
import globalize from './internal/globalize';

/**
 * Triggers a custom event on the AJS object
 *
 * @param {String} name - name of event
 * @param {Array} args - args for event handler
 */
function triggerEvt (name, args) {
    $(createElement).trigger(name, args);
}

/**
 * Binds handler to the AJS object
 *
 * @param {String} name
 * @param {Function} func
 */
function bindEvt (name, func) {
    $(createElement).bind(name, func);
}

/**
 * Some generic error handling that fires event in multiple contexts
 * - on AJS object
 * - on Instance
 * - on AJS object with prefixed id.
 *
 * @param evt
 * @param inst
 * @param args
 */
function triggerEvtForInst (evt, inst, args) {
    $(inst).trigger(evt, args);
    triggerEvt(evt, args);
    if (inst.id) {
        triggerEvt(inst.id + '-' + evt, args);
    }
}

bindEvt = deprecate.fn(bindEvt, 'bindEvt', {
    sinceVersion: '5.8.0'
});

triggerEvt = deprecate.fn(triggerEvt, 'triggerEvt', {
    sinceVersion: '5.8.0'
});

triggerEvtForInst = deprecate.fn(triggerEvtForInst, 'triggerEvtForInst', {
    sinceVersion: '5.8.0'
});

globalize('bindEvt', bindEvt);
globalize('triggerEvt', triggerEvt);
globalize('triggerEvtForInst', triggerEvtForInst);

export {
    bindEvt,
    triggerEvt,
    triggerEvtForInst
};
