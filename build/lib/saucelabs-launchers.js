// jscs:disable

var config = {
    // Chrome latest
    chrome_latest_linux: {
        browserName: 'chrome',
        platform: 'Linux'
    },
    chrome_latest_windows: {
        browserName: 'chrome',
        platform: 'Windows 10'
    },
    chrome_latest_osx: {
        browserName: 'chrome',
        platform: 'OS X 10.11'
    },

    // Firefox latest
    firefox_latest_linux: {
        browserName: 'firefox'
    },
    firefox_latest_windows: {
        browserName: 'firefox',
        platform: 'Windows 10'
    },
    firefox_latest_osx: {
        browserName: 'firefox',
        platform: 'OS X 10.11'
    },

    safari_latest_osx: {
        browserName: 'safari',
        platform: 'OS X 10.11'
    },
    ie_10: {
        browserName: 'internet explorer',
        version: '10',
        platform: 'Windows 7'
    },
    ie_11: {
        browserName: 'internet explorer',
        version: '11',
        platform: 'Windows 8.1'
    },
    ie_edge: {
        browserName: 'microsoftedge',
        version: '25.10586',
        platform: 'Windows 10'
    }
};

Object.keys(config).forEach(function (key) {
    config[key].base = 'SauceLabs';
    config[key].screenResolution = '1024x768';
});

module.exports = config;
