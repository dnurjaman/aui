'use strict';

var gat = require('gulp-auto-task');
var path = require('path');
var pkg = require('../../../package.json');
var spawn = require('child_process').spawn;
var opts = gat.opts();

module.exports = function flatappBuild (done) {
    var soyOpts = {
        baseDir: path.join('.tmp', 'flatapp', 'src', 'soy', 'pages'),
        i18nBundle: '.tmp/i18n.properties',
        glob: '**.soy',
        outDir: path.join('.tmp', 'flatapp', 'target', 'static', 'pages'),
        rootNamespace: 'testPages.pages',
        data: {
            auiVersion: pkg.version,
            language: 'en'
        },
        dependencies: [{
            // AUI soy templates
            baseDir: path.join(opts.root, 'src/soy'),
            glob: '**.soy'
        }, {
            // flatapp dependencies
            baseDir: path.join('.tmp', 'flatapp', 'src', 'soy', 'dependencies'),
            glob: '**.soy'
        }]
    };

    var params = [
        '-jar', './build/jar/atlassian-soy-cli-support-2.4.0-SNAPSHOT-jar-with-dependencies.jar',
        '--type', 'render',
        '--i18n', soyOpts.i18nBundle,
        '--basedir', soyOpts.baseDir,
        '--glob', soyOpts.glob,
        '--outdir', soyOpts.outDir
    ];

    if (soyOpts.outputExtension) {
        params = params.concat(['--extension', soyOpts.outputExtension]);
    }

    if (soyOpts.rootNamespace) {
        params = params.concat(['--rootnamespace', soyOpts.rootNamespace]);
    }

    if (soyOpts.dependencies) {
        var depString = soyOpts.dependencies.map(function(dep) {
            return dep.baseDir + ':' + dep.glob;
        }).join(',');
        params = params.concat(['--dependencies', depString]);
    }

    if (soyOpts.data) {
        var dataString = Object.keys(soyOpts.data).map(function(key) {
            return key + ':' + soyOpts.data[key];
        }).join(',');
        params = params.concat(['--data', dataString]);
    }

    var javaExec = process.env.JAVA_HOME ? process.env.JAVA_HOME + '/bin/java' : 'java';
    var cmd = spawn(javaExec, params, {stdio: 'inherit'});

    cmd.on('close', function(code) {
        if (code === 0) {
            done();
        } else {
            done(code);
        }
    });
};
