'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');
var karma = require('../lib/karma');

var opts = gat.opts();
var taskBuildTest = gat.load('test/build');

module.exports = gulp.series(
    taskBuildTest,
    function run (done) {
        karma(opts, done).start();
    }
);
