var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpBabel = require('gulp-babel');
var gulpDebug = require('gulp-debug');
var gulpIf = require('gulp-if');
var gulpSourcemaps = require('gulp-sourcemaps');
var isEs6 = require('../../lib/is-es6');
var lazyPipe = require('lazypipe');
var libVersionReplace = require('../../lib/version-replace');
var rootPaths = require('../../lib/root-paths');

module.exports = function libJs () {
    var jsPaths = rootPaths('src/{js,js-vendor}/{**/*,*}.js');
    var jsSoyPaths = rootPaths('src/js/aui-soy.js');

    var babelify = lazyPipe()
        .pipe(gulpBabel, {
            presets: ['es2015', 'react'],
            plugins: [
                'add-module-exports',
                'transform-es2015-modules-umd'
            ]
        });

    var processEs6 = lazyPipe()
        .pipe(gulpSourcemaps.init)
        .pipe(gulpDebug, {title: 'babel-umd'})
        .pipe(galv.cache, 'babel-umd', babelify())
        .pipe(gulpSourcemaps.write, '.');

    return gulp.src(jsPaths.concat(jsSoyPaths))
        .pipe(libVersionReplace())
        .pipe(gulpIf(isEs6, processEs6()))
        .pipe(gulp.dest('lib'));
};
